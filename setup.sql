CREATE DATABASE myexample;

USE myexample;

CREATE TABLE mytable (myfield VARCHAR(20));
CREATE TABLE mytable2 (myfield VARCHAR(20));

INSERT INTO mytable VALUES ('Hello'), ('Dolly');
INSERT INTO mytable2 VALUES ('Hello World'), ('Dolly 2');
