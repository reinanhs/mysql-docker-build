# mysql-docker-build

Projeto para realizar testes

## Getting started

A imagem do contêiner mysql/mariadb contém um script de inicialização que executará tudo em `/docker-entrypoint-initdb.d/`.

Documentação sobre isso: [Initializing a fresh instance](https://hub.docker.com/_/mysql/)

```ssh
docker build --tag mysql-docker-build .
docker run -d --rm --name my-container mysql-docker-build
docker logs my-container

docker run -it --rm --link my-container mysql:latest mysql -hmy-container -uroot -proot myexample -e "select * from mytable;"
```
