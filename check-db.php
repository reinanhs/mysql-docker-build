<?php
$config = [
    [
        'host' => getenv('DB_HOST'),
        'dbname' => getenv('DB_NAME'),
        'charset' => 'utf8mb4',
        'dbUser' => getenv('DB_USER'),
        'dbPass' => getenv('DB_PASS'),
        'port' => getenv('DB_PORT'),
    ],
];

function check($host, $dbname, $charset, $dbUser, $dbPass, $port){
    echo "Iniciando a verificação do banco de dados: $host \n";

    $pdo = new PDO(
        "mysql:host=$host;dbname=$dbname;charset=$charset; port=$port;",
        $dbUser,
        $dbPass,
        [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT => false,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ]
    );

    $query = "show tables";
    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo "Resultado: " . sizeof($rows) . "\n";
}

foreach ($config as $c) {
    check($c['host'], $c['dbname'], $c['charset'], $c['dbUser'], $c['dbPass'], $c['port']);
}
